from django.db import models

# Create your models here.

class Lesson(models.Model):
    nama_mata_kuliah = models.CharField(max_length = 50)
    dosen_pengajar = models.CharField(max_length = 50)

    LIST_SKS = (
        ('1', '1'),
        ('2','2'),
        ('3', '3'),
        ('4','4'),
        ('5', '5'),
        ('6','6'),
    )

    jumlah_sks = models.CharField(
        max_length=100,
        choices = LIST_SKS,
        default = '1',
    )
    deskripsi_mata_kuliah = models.TextField(blank=False, null=False)

    LIST_SEMESTER = (
        ('Ganjil 2019/2020', 'Ganjil 2019/2020'),
        ('Genap 2019/2020','Genap 2019/2020'),
    )

    semester_tahun = models.CharField(
        max_length=100,
        choices = LIST_SEMESTER,
        default = 'Ganjil 2019/2020',
        )
    ruang_kelas = models.CharField(max_length = 10)

    published = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)

    def __str__(self):
        return "{}. {}".format(self.id, self.nama_mata_kuliah)