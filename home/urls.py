from django.urls import path
from django.conf.urls import url 

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('newpage/', views.newpage, name='newpage'),
    path('jadwal/', views.jadwal, name='jadwal'),
    path('delete/(?P<delete_id>[0-9]$)', views.delete, name='delete'),
    path('detail/<int:detail_id>', views.detail, name='detail'),
]