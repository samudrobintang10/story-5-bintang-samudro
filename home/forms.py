from django import forms


# import model dari models.py
from .models import Lesson

# class PostForm(forms.Form):
#     # nama_mata_kuliah = forms.CharField(max_length = 30)
#     # dosen_pengajar = forms.CharField(max_length = 30)
#     # jumlah_sks = forms.IntegerField()
#     # deskripsi_mata_kuliah = forms.CharField(
#     #     widget = forms.Textarea
#     # )
#     # semester_tahun = forms.CharField(max_length = 20)
#     # ruang_kelas = forms.CharField(max_length = 10)

class PostForm(forms.ModelForm):
    class Meta:
        model = Lesson
        fields = [
            'nama_mata_kuliah',
            'dosen_pengajar',
            'jumlah_sks',
            'deskripsi_mata_kuliah',
            'semester_tahun',
            'ruang_kelas',
        ]

        labels = {
            'nama_mata_kuliah':'Nama Mata Kuliah',
            'dosen_pengajar':'Dosen Pengajar',
            'jumlah_sks':'Jumlah SKS',
            'deskripsi_mata_kuliah':'Deskripsi Mata Kuliah',
            'semester_tahun':'Semester dan Tahun ajaran',
            'ruang_kelas':'Ruang Kelas',
        }

        widgets = {
            'nama_mata_kuliah': forms.TextInput(
                attrs= {
                    'class':'form-control',
                    'placeholder':'Contoh: Dasar-Dasar Pemograman 1',
                }
            ),
            'dosen_pengajar': forms.TextInput(
                attrs= {
                    'class':'form-control',
                    'placeholder':'Contoh: Bintang Samudro S.Kom',
                }
            ),
            'jumlah_sks': forms.Select(
                attrs= {
                    'class':'form-control',
                }
            ),
            'deskripsi_mata_kuliah': forms.Textarea(
                attrs= {
                    'class':'form-control',
                    'placeholder':'Contoh: apa aja bre',
                }
            ),
            'semester_tahun': forms.Select(
                attrs= {
                    'class':'form-control',
                }
            ),
            'ruang_kelas': forms.TextInput(
                attrs= {
                    'class':'form-control',
                    'placeholder':'Contoh: 2.2406',
                }
            ),
        }