from django.shortcuts import render, redirect

from .forms import PostForm
from .models import Lesson

nama_pribadi = "Bintang Samudro"

# Create your views here.

def delete(request, delete_id):
    Lesson.objects.filter(id=delete_id).delete()
    return redirect('home:jadwal')


def index(request):
    response = {'name' : nama_pribadi}
    return render(request, 'home/index.html', response)

def newpage(request):
    post_form = PostForm(request.POST or None)
    # error = None

    if request.method == 'POST':

        if post_form.is_valid():
            post_form.save()

            return redirect('home:jadwal')

    context = {
        'page_title':'Susun Jadwal',
        'post_form':post_form,
    }
    return render(request, 'home/newpage.html', context)

def jadwal(request):
    posts = Lesson.objects.all()
    context = {
        'page_title':'List Mata Kuliah',
        'posts':posts,
    }

    return render(request, 'home/jadwal.html', context)

def detail(request, detail_id):
    id_mata_kuliah = Lesson.objects.get(id=detail_id)
    context = {
        'mata_kuliah': id_mata_kuliah,
        'page_title':'Deskripsi Mata Kuliah',
    }
    return render(request, 'home/detail.html', context)